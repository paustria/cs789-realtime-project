#include <iostream>
#include "Task.h"
#include "Taskset.h"

int main() {
    int time = 0;
    int time_slice = 1;
    const int NUM_TASKS = 2;

    Task task_1(1,0,25,25,5,1);
    Task task_2(2,0,35,35,10,2);
    Task task_3(3,0,45,45,20,3);
    Taskset starting_ts("starting");
    Taskset ready_ts("ready");
    Taskset waiting_ts("waiting");
    Taskset exec_ts("executing");

    starting_ts.add_task(task_1);
    starting_ts.add_task(task_2);
    /* starting_ts.add_task(task_3); */
    starting_ts.sort_by_priority();

    // execute first job
    Task execTask = starting_ts.remove_task(); // reuse variable
    exec_ts.add_task(execTask);

    // set time to arrival of first task
    time = exec_ts.top().get_phase();
    exec_ts.top().execute(time_slice);
    time = time + time_slice;

    while (true) {
        // check arrival of taskset
        if (starting_ts.get_num_tasks() > 0) {
            for (auto it = starting_ts.tasks.begin(); it != starting_ts.tasks.end(); it++) {
                if (time >= it->get_phase()) {
                    ready_ts.add_task(*it);
                    starting_ts.tasks.erase(it);
                    it--;
                }
            }
        }

        // check waiting tasks
        if (waiting_ts.get_num_tasks() > 0) {
            for (auto it = waiting_ts.tasks.begin(); 
                    it != waiting_ts.tasks.end(); it++) {

                // subtract waiting time
                int new_wait_time = it->get_wait_time() - time_slice;
                it->set_wait_time(new_wait_time);

                // move to ready queue when wait time is zero
                if (it->get_wait_time() == 0) {
                    // reset wait time
                    it->reset_wait_time();
                    std::cout << "moving task " << it->get_id() << " to ready queue at time " << time << "\n";
                    ready_ts.add_task(*it);
                    waiting_ts.tasks.erase(it);
                    it--;
                }
            }
        }

        /* // sort more than two jobs in ready queue */
        if (ready_ts.get_num_tasks() > 1) {
            ready_ts.sort_by_priority();
        }

        // no task in execution queue
        if (exec_ts.get_num_tasks() == 0 && ready_ts.get_num_tasks() > 0) {
            std::cout << "execution queue empty, move task " << ready_ts.tasks.back().get_id() << " at time " << time << "\n";
            execTask = ready_ts.remove_task();
            exec_ts.add_task(execTask);

        // task in execution queue
        } else if (exec_ts.get_num_tasks() > 0 && 
                ready_ts.get_num_tasks() > 0) {
            // preempt lower priority task
            if (exec_ts.top().get_priority() > ready_ts.top().get_priority()) {
                std::cout << "preempt task id " << exec_ts.top().get_id() << "\n";

                Task preemtedTask = exec_ts.remove_task();
                preemtedTask.reset_remaining_exec();

                execTask = ready_ts.remove_task();
                exec_ts.add_task(execTask);
                ready_ts.add_task(preemtedTask);
            }
        }

        // execute tasks
        if (exec_ts.get_num_tasks() > 0) {
            // execute
            std::cout << "execute task id: " << exec_ts.top().get_id() << " at time: " << time << "\n";
            exec_ts.top().execute(time_slice);

            // check remaining execution
            if (exec_ts.top().get_remaining_exec() <= 0) {
                std::cout << "task id: " << exec_ts.top().get_id() 
                    << " complete at time " << time << "\n";

                // reset execution time
                execTask = exec_ts.remove_task();
                execTask.reset_remaining_exec();
                waiting_ts.add_task(execTask);
                
                if (ready_ts.get_num_tasks() > 0) {
                    execTask = ready_ts.remove_task();
                    exec_ts.add_task(execTask);
                    std::cout << "loaded task id " << exec_ts.top().get_id() 
                        << " into exectuion queue at time " << time << "\n";
                }
            }
        } // end execute task

        time = time + time_slice;
    }
    
    return 0;
}
