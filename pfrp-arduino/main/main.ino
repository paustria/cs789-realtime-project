#include <Task.h>
#include <Taskset.h>

int task_1_pin = 10;
int task_2_pin = 11;
int task_comp_pin = 9;
int abort_pin = 6;
int time = 0;
int time_slice = 1;

Task task_1(1,0,25,25,5,1,10);
Task task_2(2,0,35,35,10,2,11);

Taskset starting_ts("starting");
Taskset ready_ts("ready");
Taskset waiting_ts("waiting");
Taskset exec_ts("executing");

  

void setup() {
  // put your setup code here, to run once
  Serial.begin(9600);
  pinMode(task_1_pin, OUTPUT);
  pinMode(task_2_pin, OUTPUT);
  pinMode(task_comp_pin, OUTPUT);
  pinMode(abort_pin, OUTPUT);

  starting_ts.add_task(task_1);
  starting_ts.add_task(task_2);
  
  Task execTask = starting_ts.remove_task(); // reuse variable
  exec_ts.tasks.push_back(execTask);
  time = exec_ts.top().get_phase();
  exec_ts.top().execute(time_slice);
  int pin = exec_ts.top().get_pin();
  digitalWrite(pin, HIGH);
  Serial.println(execTask.get_id());
}

void loop() {
  // put your main code here, to run repeatedly:
 
 

  
  time = time + time_slice;
}
