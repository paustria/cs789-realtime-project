#ifndef Taskset_h
#define Taskset_h

#include <Task.h>
#include <Vector.h>


class Taskset {
    private:
        String _name;
    public:
        Vector<Task> tasks;
        Taskset(String);
        void sort_by_priority();
        Task remove_task();
        void add_task(Task);
        int get_num_tasks();
        Task& top();
};

#endif
