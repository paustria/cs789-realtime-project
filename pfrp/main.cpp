#include <iostream>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <vector>
#include "Task.h"
#include "Taskset.h"

int main() {
    std::vector<int> task_data;
    std::string line, word;
    std::fstream fin;
    fin.open("tasks.csv", std::ios::in);

    // read in dataset
    while (!fin.eof()) {
        getline(fin, line);
        std::stringstream s(line);

        while (getline(s, word, ',')) {
            task_data.push_back(stoi(word));
        }
    }
    fin.close();

    int time = 0;
    int time_slice = 1;
    const int TASK_SETS = 100;
    const int TASKS_PER_TASKSET = 5;
    const int TASK_ITEMS = 6;
    bool schedulable = true;
    int schedule_tasks = 0;
    

    for (int taskset = 0; taskset < TASK_SETS; taskset++) {

        // create taskset queues
        Taskset starting_ts("starting");
        Taskset ready_ts("ready");
        Taskset waiting_ts("waiting");
        Taskset exec_ts("executing");
        Taskset completed_ts("completed");

        // create tasks from taskset 
        for (int task = 0; task < TASKS_PER_TASKSET; task++) {
            int taskset_idx = taskset * TASKS_PER_TASKSET * TASK_ITEMS;
            int task_item_idx = task * TASK_ITEMS;

            int id = task_data[taskset_idx + task_item_idx];
            int phase = task_data[taskset_idx + task_item_idx + 1];
            int period = task_data[taskset_idx + task_item_idx + 2];
            int deadline = task_data[taskset_idx + task_item_idx + 3];
            int wcet = task_data[taskset_idx + task_item_idx + 4];
            int priority = task_data[taskset_idx + task_item_idx + 5];

            /* std::printf("%d %d %d %d %d %d", id, phase, period, deadline, wcet, priority); */
            /* std::cout << "\n"; */
            starting_ts.add_task(Task(id, phase, period, deadline, wcet, priority));
            std::printf("Taskset %d task %d added to starting queue\n", taskset, id);
        }
        starting_ts.sort_by_priority();

        // TEST tasks
        /* Task task_1(1,1433,4980,4980,53,5); */
        /* Task task_2(2,3571,3750,3750,69,4); */
        /* Task task_3(3,1488,3070,3070,57,3); */
        /* Task task_4(4,173,1560,1560,60,2); */
        /* Task task_5(5,675,1110,1110,25,1); */

        /* starting_ts.add_task(task_1); */
        /* starting_ts.add_task(task_2); */
        /* starting_ts.add_task(task_3); */
        /* starting_ts.add_task(task_4); */
        /* starting_ts.add_task(task_5); */

        // execute first job
        Task execTask = starting_ts.remove_task(); // reuse variable
        exec_ts.add_task(execTask);

        // set time to arrival of first task
        time = exec_ts.top().get_phase();
        exec_ts.top().execute(time_slice);
        time = time + time_slice;

        while (schedulable && (completed_ts.get_num_tasks() < TASKS_PER_TASKSET)) {
            // check arrival of taskset
            if (starting_ts.get_num_tasks() > 0) {
                for (auto it = starting_ts.tasks.begin(); it != starting_ts.tasks.end(); it++) {
                    if (time >= it->get_phase()) {
                        it->set_abs_deadline(time);
                        ready_ts.add_task(*it);
                        starting_ts.tasks.erase(it);
                        it--;
                    }
                }
            }

            // check if ready tasks have missed their deadline
            if (ready_ts.get_num_tasks() > 0) {
                for (auto it = ready_ts.tasks.begin(); it != ready_ts.tasks.end(); it++) {
                    if (time > it->get_abs_deadline()) {
                        std::cout << "\ntask " << it->get_id() << " is not schedulable!\n";
                        std::cout << "task " << it->get_id() << " deadline is " << it->get_abs_deadline() << ", time is " << time << "\n";
                        schedulable = false;
                    }
                }
            }


            // check waiting tasks
            if (waiting_ts.get_num_tasks() > 0) {
                for (auto it = waiting_ts.tasks.begin(); 
                        it != waiting_ts.tasks.end(); it++) {

                    // subtract waiting time
                    int new_wait_time = it->get_wait_time() - time_slice;
                    it->set_wait_time(new_wait_time);

                    // move to ready queue when wait time is zero
                    if (it->get_wait_time() == 0) {
                        // reset wait time
                        it->reset_wait_time();
                        std::cout << "moving task " << it->get_id() << " to ready queue at time " << time << "\n";
                        it->set_abs_deadline(time);
                        ready_ts.add_task(*it);
                        waiting_ts.tasks.erase(it);
                        it--;
                    }
                }
            }

            /* // sort more than two jobs in ready queue */
            if (ready_ts.get_num_tasks() > 1) {
                ready_ts.sort_by_priority();
            }

            // no task in execution queue
            if (exec_ts.get_num_tasks() == 0 && ready_ts.get_num_tasks() > 0) {
                std::cout << "execution queue empty, move task " << ready_ts.tasks.back().get_id() << " at time " << time << "\n";
                execTask = ready_ts.remove_task();
                exec_ts.add_task(execTask);

            // task in execution queue
            } else if (exec_ts.get_num_tasks() > 0 && 
                    ready_ts.get_num_tasks() > 0) {
                // preempt lower priority task
                if (exec_ts.top().get_priority() > ready_ts.top().get_priority()) {
                    std::cout << "preempt task id " << exec_ts.top().get_id() << "\n";

                    Task preemtedTask = exec_ts.remove_task();
                    preemtedTask.reset_remaining_exec();

                    execTask = ready_ts.remove_task();
                    exec_ts.add_task(execTask);
                    ready_ts.add_task(preemtedTask);
                }
            }

            // execute tasks
            if (exec_ts.get_num_tasks() > 0) {
                // execute
                //std::cout << "execute task id: " << exec_ts.top().get_id() << " at time: " << time << "\n";
                exec_ts.top().execute(time_slice);

                // check remaining execution
                if (exec_ts.top().get_remaining_exec() <= 0) {
                    std::cout << "task id: " << exec_ts.top().get_id() 
                        << " complete at time " << time << "\n";

                    
                    // check if completed tasks exist
                    if (completed_ts.get_num_tasks() > 0) {
                        bool task_exist = false;

                        for (auto it = completed_ts.tasks.begin(); 
                                it != completed_ts.tasks.end(); it++) {

                            if (it->get_id() == exec_ts.top().get_id()) {
                                task_exist = true;
                                break;
                            }

                        }

                        if (!task_exist) {
                            std::cout << "task " << exec_ts.top().get_id() << " added to completed queue " << "\n";
                            completed_ts.add_task(exec_ts.top());
                        }
                    } else {
                        completed_ts.add_task(exec_ts.top());
                    }
                    

                    // reset execution time
                    execTask = exec_ts.remove_task();
                    execTask.reset_remaining_exec();
                    waiting_ts.add_task(execTask);
                    
                    if (ready_ts.get_num_tasks() > 0) {
                        execTask = ready_ts.remove_task();
                        exec_ts.add_task(execTask);
                        std::cout << "loaded task id " << exec_ts.top().get_id() 
                            << " into exectuion queue at time " << time << "\n";
                    }
                }
            } // end execute task

            time = time + time_slice;
        }

        if (schedulable) {
            std::printf("\nTaskset %d is schedulable!\n", taskset);
            schedule_tasks++;
        } else {
            std::printf("\nTaskset %d is not schedulable!\n", taskset);
        } 
    } // end for loop

    std::printf("\nSchedulable Tasks: %d\n", schedule_tasks);
    std::printf("Total Tasks: %d\n\n", TASK_SETS);

    return 0;
}
