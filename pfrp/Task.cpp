#include "Task.h"

Task::Task(
    int id, 
    int phase, 
    int period, 
    int deadline,
    int wcet, 
    int priority
) {
    _id = id;
    _phase = phase;
    _period = period;
    _deadline = deadline;
    _wcet = wcet;
    _priority = priority;
    _remaining_exec = wcet;
    _wait_time = period;
}

void Task::execute(int time_slice) {
    _remaining_exec -= time_slice;
}

int Task::get_id() {
    return _id;
}

int Task::get_phase() {
    return _phase;
}

int Task::get_period() {
    return _period;
}

int Task::get_wcet() {
    return _wcet;
}

int Task::get_priority() {
    return _priority;
}

void Task::set_priority(int p) {
    _priority = p;
}

int Task::get_remaining_exec() {
    return _remaining_exec;
}

void Task::set_remaining_exec(int re) {
    _remaining_exec = re;
}

void Task::reset_remaining_exec() {
    _remaining_exec = _wcet;
}

int Task::get_wait_time() {
    return _wait_time;
}

void Task::set_wait_time(int wt) {
    _wait_time = wt;
}

void Task::reset_wait_time() {
    _wait_time = _period;
}

float Task::get_utilization() {
    return _utlization;
}

void Task::calc_utilization() {
    _utlization = _wcet / _period;
}

int Task::get_abs_deadline() {
    return _abs_deadline;
}

void Task::set_abs_deadline(int time) {
    _abs_deadline = time + _deadline;
}
