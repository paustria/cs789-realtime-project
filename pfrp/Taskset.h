#ifndef Taskset_h
#define Taskset_h

#include "Task.h"
#include <vector>
#include <string>


class Taskset {
    private:
        std::string _name;
    public:
        std::vector<Task> tasks;
        Taskset(std::string);
        void print_task_ids();
        void sort_by_priority();
        Task remove_task();
        void add_task(Task);
        int get_num_tasks();
        Task& top();
};

#endif
