#include <algorithm>
#include <iostream>
#include "Taskset.h"

Taskset::Taskset(std::string name) {
    _name = name;
}

void Taskset::print_task_ids() {
    std::cout << _name <<" task ids: ";
    for (auto i = tasks.begin(); i != tasks.end(); i++) {
        std::cout << i->get_id() << " ";
    }
    std::cout << "\n";
}

// decreasing order
void Taskset::sort_by_priority() {
    std::sort(
        tasks.begin(),
        tasks.end(),
        [](Task& lhs, Task& rhs) {
        
        return lhs.get_priority() > rhs.get_priority();
    });
}

Task Taskset::remove_task() {
    Task task = tasks.back();
    tasks.pop_back();
    return task;
}

void Taskset::add_task(Task t) {
    tasks.push_back(t);
}

int Taskset::get_num_tasks() {
    return tasks.size();
}

Task& Taskset::top() {
    return tasks.back();
}
