#ifndef Task_h
#define Task_h

class Task {
    public:
        Task(int id, int phase, int period, int deadline, int wcet, int priority);

        void execute(int);
        int get_id();
        int get_phase();
        int get_period();
        int get_deadline();
        int get_wcet();
        int get_priority();
        void set_priority(int);
        int get_remaining_exec();
        void set_remaining_exec(int);
        void reset_remaining_exec();
        int get_wait_time();
        void set_wait_time(int);
        void reset_wait_time();
        float get_utilization();
        void calc_utilization();
        int get_abs_deadline();
        void set_abs_deadline(int);
    private:
        int _id;
        int _phase;
        int _period;
        int _deadline;
        int _wcet;
        int _priority;
        int _remaining_exec;
        int _wait_time;
        float _utlization;
        int _abs_deadline;
};

#endif
