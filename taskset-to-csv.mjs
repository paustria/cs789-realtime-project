'use strict'

import fs from 'fs'

if (process.argv.length !== 3) {
  console.log('usage: node <program> <taskset.json>')
  process.exit(1)
}

const FILE_NAME = process.argv[2]

const tasksJson = fs.readFileSync(FILE_NAME)
const data = JSON.parse(tasksJson).data
let tasksets = data.tasksets

tasksets.forEach(taskset => {
  // remove 0th job
  const removeTask = tasks => tasks.slice(1)
  taskset.tasks = removeTask(taskset.tasks)
  taskset.tasks = rankByPeriod(taskset.tasks)
  taskset.tasks = sortByPriority(taskset.tasks)

  taskset.tasks.forEach(task => {
    console.log(`${task.id},${task.phase},${task.period},${task.deadline},${task.wcet},${task.priority}`)
  })
})



/* ------------------------- Helper Functions --------------------------- */
function rankByPeriod(tasks) {
  // sort period min to max
  let periods = tasks.map(task => {
    return task.period
  })
  periods = periods.sort((a, b) => a - b)

  // object for ranking
  let periodRank = {} 
  let rank = 0
  periods.forEach((period, i) => {
    if (i == 0) {
      rank++
    } else if (period !== periods[i-1]) {
      rank++
    }
    periodRank[period] = rank
  })

  // update task's priority
  return tasks.map(task => {
    task.priority = periodRank[task.period]
    return task
  })
}

function sortByPriority(tasks) {
  const newTasks = tasks.sort((a, b) => b.priority < a.priority ? 1 : -1)
  return newTasks
}
