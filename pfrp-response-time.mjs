'use strict'

import fs from 'fs'

if (process.argv.length !== 3) {
  console.log('usage: node <program> <taskset.json>')
  process.exit(1)
}

const FILE_NAME = process.argv[2]

const tasksJson = fs.readFileSync(FILE_NAME)
const data = JSON.parse(tasksJson).data
let tasksets = data.tasksets

// for calculating scedulability percentage
const numTasksets = tasksets.length
let numSchedulableTasksets = 0

// calculate reponse time for each job
tasksets.forEach(taskset => {
  // remove 0th job
  const removeTask = tasks => tasks.slice(1)
  taskset.tasks = removeTask(taskset.tasks)

  taskset.tasks = calcTaskUtilization(taskset.tasks)
  taskset.utilization = calcTasksetUtilization(taskset.tasks)
  //taskset.tasks = rankByPeriod(taskset.tasks)
  //taskset.tasks = rankByUtilization(taskset.tasks)
  taskset.tasks = rankByExecution(taskset.tasks)
  taskset.tasks = sortByPriority(taskset.tasks)

  taskset.tasks.forEach((task, i) => {
    // highest priority task
    if (i === 0) {
      taskset.tasks[i].responseTime = task.wcet
    } else {
      const hpTasks = taskset.tasks.slice(0, i) // tasks are sorted

      // iterate through high priority task to find response time
      let responseTime = 0
      for (let Ri = 0; Ri <= i+1; Ri++) {

        if (Ri === 0) {
          responseTime = task.wcet
        } else {
          let sumResponseTime = 0
          hpTasks.forEach(hpTask => {
            // do not include task that have same priority
            if (hpTask.priority < taskset.tasks[i].priority) {
              // find max WCET
              const kTasks = taskset.tasks.filter(kTask => {
                return (taskset.tasks[i].priority >= kTask.priority) &&
                  (kTask.priority > hpTask.priority)
              })

              let maxCk = 0
              kTasks.forEach(kTask => {
                if (kTask.wcet > maxCk ) {
                  maxCk = kTask.wcet
                }
              })

              const newWCET = hpTask.wcet + maxCk
              sumResponseTime += Math.ceil(responseTime/hpTask.period) * newWCET
            } // end if
          }) // end for each
          responseTime = task.wcet + sumResponseTime
        } // end else
      } // end for loop
      taskset.tasks[i].responseTime = responseTime
    }
  }) // end for each

  // check if taskset is schedulable
  let schedulable = true
  taskset.tasks.forEach(task => {
    console.log(`task id: ${task.id}, utilization: ${task.utilization.toFixed(3)}, response time: ${task.responseTime}, deadline: ${task.deadline}`)
    if (task.responseTime > task.deadline) {
      schedulable = false
    }
  })

  console.log(`\ntaskset id: ${taskset.id}, utilization: ${taskset.utilization.toFixed(3)}`)
  if (schedulable) {
    console.log('taskset is schedulable!\n')
    numSchedulableTasksets++
  } else {
    console.log('taskset is not schedulable!\n')
  }
}) // end for each


console.log(`\n\ntotal tasksets: ${numTasksets},
schedulable tasksets: ${numSchedulableTasksets},
schedulability: ${(numSchedulableTasksets/numTasksets).toFixed(2)*100}%`)

/* ------------------------- Helper Functions --------------------------- */
function calcTaskUtilization(tasks) {
  return tasks.map(task => {
    task.utilization = task.wcet / task.period
    return task
  })
}

function calcTasksetUtilization(tasks) {
  let sumUtil = 0
  tasks.forEach(task => {
    sumUtil = sumUtil + task.utilization
  })
  return sumUtil
}

function rankByPeriod(tasks) {
  let periods = tasks.map(task => {
    return task.period
  })
  // sort period max to min (a - b)
  periods = periods.sort((a, b) => a - b)

  // object for ranking
  let periodRank = {} 
  let rank = 0
  periods.forEach((period, i) => {
    if (i == 0) {
      rank++
    } else if (period !== periods[i-1]) {
      rank++
    }
    periodRank[period] = rank
  })

  // update task's priority
  return tasks.map(task => {
    task.priority = periodRank[task.period]
    return task
  })
}

function rankByUtilization(tasks) {
  let utilizations = tasks.map(task => {
    return task.utilization
  })
  // sort utilization max to min (b - a)
  utilizations = utilizations.sort((a, b) => b - a)

  // object for ranking
  let utilizationRank = {} 
  let rank = 0
  utilizations.forEach((util, i) => {
    if (i == 0) {
      rank++
    } else if (util !== utilizations[i-1]) {
      rank++
    }
    utilizationRank[util] = rank
  })

  // update task's priority
  return tasks.map(task => {
    task.priority = utilizationRank[task.utilization]
    return task
  })
}

function rankByExecution(tasks) {
  let executions = tasks.map(task => {
    return task.wcet
  })
  // sort wcet max to min (b - a)
  executions = executions.sort((a, b) => b - a)

  // object for ranking
  let executionRank = {} 
  let rank = 0
  executions.forEach((exec, i) => {
    if (i == 0) {
      rank++
    } else if (exec !== executions[i-1]) {
      rank++
    }
    executionRank[exec] = rank
  })

  // update task's priority
  return tasks.map(task => {
    task.priority = executionRank[task.wcet]
    return task
  })
}

function sortByPriority(tasks) {
  const newTasks = tasks.sort((a, b) => b.priority < a.priority ? 1 : -1)
  return newTasks
}
